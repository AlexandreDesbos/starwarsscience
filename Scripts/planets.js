
$(document).ready(function(){

   var listPlanetNumber = [1,3,4,6,7,8,10,11,14,17,37,61];
   var count = 0;

    getAndWriteData(listPlanetNumber[count]);

    function getAndWriteData(counter){
        $.get('https://swapi.co/api/planets/'+counter,function(dataPlanet){
            document.getElementById('planetName').innerHTML = dataPlanet.name;
            document.getElementById('climate').innerHTML = dataPlanet.climate;
            document.getElementById('population').innerHTML = dataPlanet.population + ' Inhabitants';
            document.getElementById('orbitalPeriod').innerHTML = dataPlanet.orbital_period + ' Days';
            document.getElementById('rotationPeriod').innerHTML = dataPlanet.rotation_period + ' Hours ';
        },"json");

    }


   $('#changeNext').click(function(){

       if (count < listPlanetNumber.length-1){
           count++;

       }else{
           count = listPlanetNumber.length-1;

       }
       // console.log(listPlanetNumber[count]);

       console.log("count ="+count);
       console.log("listPlanetNumber[count] = " +listPlanetNumber[count]);
       console.log("length = "+listPlanetNumber.length);
       getAndWriteData(listPlanetNumber[count]);
       changePhoto(listPlanetNumber[count]);
       //checkBorder(count);


   });

   $('#changePrevious').click(function () {

       if(count>0){
           count--;
       }
       else{
           count = 0;
       }
      getAndWriteData(listPlanetNumber[count]);
      changePhoto(listPlanetNumber[count]);


   });

   $('#processRandom').click(function () {


       let EnteredNumber = document.getElementById("EnteredNumber").value;
       if (EnteredNumber === "" || EnteredNumber < 0){
           alert("Enter a integer between 0 and +∞")
       }else {
           EnteredNumber = EnteredNumber % 12;
           getAndWriteData(listPlanetNumber[EnteredNumber]);
           changePhoto(listPlanetNumber[EnteredNumber]);
           count = EnteredNumber;
           console.log('count = ' + count);
       }
      document.getElementById("EnteredNumber").value = '';
   });



    function changePhoto(counter){

        $("#picContainer").css({'background-image':'url(../Photos/Planets/'+counter+'.png)'});

    }



});

