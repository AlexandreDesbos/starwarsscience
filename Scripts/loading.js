var intObj = {
    template: 3,
    parent: '#chart-container' // this option will insert bar HTML into this parent Element
};
var indeterminateProgress = new Mprogress(intObj);
indeterminateProgress.start();
$(document).ajaxStart(function () {
    $('#loadingAJAX').show();


});
$(document).ajaxStop(function () {
    $('#loadingAJAX').hide();
    indeterminateProgress.end();

});

