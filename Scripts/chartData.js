$(document).ready(function () {

    let listPlanetNumber = [1,3,4,6,7,8,10,14,17,37,61];
    let planetPop = [];
    let planetName =[];
    let i = 0;



    function getDataAndDraw(callback) {
        for ( i ; i < listPlanetNumber.length ; i++){

            $.ajaxSetup({async:false});
            $.get('https://swapi.co/api/planets/'+listPlanetNumber[i], function(dataPlanet){
                // listPlanet[i] = new Planet(dataPlanet.name, dataPlanet.population);
                planetName[i] = dataPlanet.name;
                planetPop[i] = dataPlanet.population;
            },"json");


        }
        // console.log(planetName);
        // console.log(planetPop);
        callback();
    }

     getDataAndDraw(function () {

         let i = 0;
         var ctx = document.getElementById('planetSizeChart').getContext('2d');
         var chart = new Chart(ctx, {
             // The type of chart we want to create
             type: 'doughnut',

             // The data for our dataset
             data: {
                 labels: planetName,
                 datasets: [{
                     label: 'Pie chart of planet\'s population ',
                     backgroundColor: ['rgb(127,107,88)','rgb(204,255,140)','rgb(200,200,200)',
                         'rgb(255,183,120)','rgb(89,199,85)','rgb(17,56,65)',
                         'rgb(46,36,255)','rgb(19,106,43)','rgb(166,90,177)',
                        'rgb(149,215,255)'],

                     data: planetPop
                 }]
             },

             // Configuration options go here
             options: {}
         });

     });

});
